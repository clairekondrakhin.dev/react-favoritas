import "./App.css";
import Section from "./components/section/Section";
import SectionMenu from "./components/sectionMenu/SectionMenu"

function App() {
  return (
    <>
      <Section/>
      <SectionMenu sectionMenuTitle='Los mejores menús' sectionMenuSubtitle="Aquí están los mejores menús de la semana, y decide que vas a pedir"/>
    </>
  );
}

export default App;
