import './Menu.style.css'

function Menu(props) {
    let { menuImage, menuRate, menuRestaurantName, menuDeliveryTime, menuCategory } = props


    return (
        <div className='Menus'>
            <img className='menu-picture' src={menuImage} alt="photo menu" />
            <div className='menu-rate'>{menuRate}</div>
            <div className="container-menuDetails">
                <p className='menu-retaurantName'>{menuRestaurantName}</p>
                <p className='menu-delivery'>{menuDeliveryTime}</p>
                <p className='menu-category'>{menuCategory}</p>
            </div>
        </div>
    );
};

export default Menu;