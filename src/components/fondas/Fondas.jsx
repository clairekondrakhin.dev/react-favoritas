// photo
// notation sur photo
// titre du restaurant
// temps de livraison
// catégorie
import PropTypes from "prop-types";
import "./Fondas.style.css";

function Fondas({ urlImg, title, rate, time, category }) {
  return (
    <div className="fondas-container">
      <img src={urlImg} alt="" />
      <div className="rate">
        <span className="material-symbols-outlined">star</span>
        {rate}
      </div>
      <h2>{title}</h2>
      <div className="p-fondas">
          <p className="time">{time}min</p>
          <p className="category">{category}</p>
      </div>
    </div>
  );
}

Fondas.propTypes = {
  urlImg: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  rate: PropTypes.number.isRequired,
  time: PropTypes.number.isRequired,
  category: PropTypes.string.isRequired,
};

export default Fondas;
