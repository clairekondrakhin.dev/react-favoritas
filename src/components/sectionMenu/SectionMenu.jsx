import './sectionMenu.css'
import Menu from "../menu/Menu";


function SectionMenu(props) {

    return (
        <>
            <button></button>
            <h1 className="section-menuTitle">
                {props.sectionMenuTitle}
            </h1>
            <span className="section-menuSubtitle">{props.sectionMenuSubtitle}</span>
            <div className="section-menuList">
                <Menu
                    menuImage="src/assets/menu-desayunos.jpeg"
                    menuRate="9.8"
                    menuRestaurantName="Doña Laura"
                    menuDeliveryTime="20-30min"
                    menuCategory="fondita"
                />

                <Menu
                    menuImage="src/assets/menu-ensaladish.jpeg"
                    menuRate="8.5"
                    menuRestaurantName="Rosa Cafe"
                    menuDeliveryTime="~45 min"
                    menuCategory="Lonchería"
                />

                <Menu
                    menuImage="src/assets/menu-hot-caketerias.jpeg"
                    menuRate="7.3"
                    menuRestaurantName="Le cottidiene"
                    menuDeliveryTime="15-20 min"
                    menuCategory="Sushi"
                />

                <Menu
                    menuImage="src/assets/menu-otros-desayunos.jpeg"
                    menuRate="8.0"
                    menuRestaurantName="Querreque"
                    menuDeliveryTime="~50 min"
                    menuCategory="Veggies"
                />

                <Menu
                    menuImage="src/assets/menu-pastish.jpeg"
                    menuRate="7.3"
                    menuRestaurantName="Le cottidiene"
                    menuDeliveryTime="15-20 min"
                    menuCategory="Sushi"
                />

                <Menu
                    menuImage="src/assets/menu-pizzeria.jpeg"
                    menuRate="8.0"
                    menuRestaurantName="Querreque"
                    menuDeliveryTime="~50 min"
                    menuCategory="Veggies"
                />
            </div>
        </>
    );

}
export default SectionMenu