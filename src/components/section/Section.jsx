import Favoritas from "../favoritas/Favoritas";
import './Section.style.css';

function Section() {
  return (
    <div className="sectionFavoritas">
      <h1>Fondas cercanas</h1>
      <Favoritas
        logoFood="src/assets/category-hamburger.png"
        numbersOfFavourites="28"
        typeOfFoodName="Hamburgesas"
        minimumPrice="Desde 50"
      />
      <Favoritas
        logoFood="src/assets/category-pizza.png"
        numbersOfFavourites="42"
        typeOfFoodName="Pizza"
        minimumPrice="Desde 60"
      />

      <Favoritas
        logoFood="src/assets/category-sushi.png"
        numbersOfFavourites="35"
        typeOfFoodName="Sushi"
        minimumPrice="Desde 50"
      />

      <Favoritas
        logoFood="src/assets/category-veggie.png"
        numbersOfFavourites="23"
        typeOfFoodName="Veggie"
        minimumPrice="Desde 50"
      />
      <Favoritas
        logoFood="src/assets/category-soup.png"
        numbersOfFavourites="15"
        typeOfFoodName="Soppas"
        minimumPrice="Desde 50"
      />
      <Favoritas
        logoFood="src/assets/category-dessert.png"
        numbersOfFavourites="9"
        typeOfFoodName="Postre"
        minimumPrice="Desde 50"
      />
    </div>
  );
}

export default Section;
