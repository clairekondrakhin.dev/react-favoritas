import './Favoritas.style.css'


function Favoritas(props) {

    return (
        <div className="favoritas-container">
            <section>
                <div className="whiteCircle">
                    <div className="logoFood">
                        <img src={props.logoFood} />
                    </div>
                </div>
                <div className="greenCircle">{props.numbersOfFavourites}</div>
            </section>
            <span className="typeOfFoodName">{props.typeOfFoodName}</span>
            <span className="minimumPrice">Desde ${props.minimumPrice}</span>
        </div>
    );
}

export default Favoritas